//déclaration de la variable number

let number = 0;

// boucle pour incrémenter number

for (let i = 0; i <= 100; i++) {
    number = i;

    //conditions 

    if (number % 3 === 0 && number % 5 === 0) { // vérifie si number est multiple de 3 et 5
        console.log("FizzBuzz"); //affiche FizzBuzz dans la console

    } else if (number % 3 === 0) { // vérifie si number est multiple de 3 seulement
        console.log("Fizz"); //affiche Fizz dans la console

    } else if (number % 5 === 0) { // vérifie si number est multiple de 5 seulement
        console.log("Buzz"); //affiche Buzz dans la console

    } else { //tous les autres cas (ni multiple de 3 ni de 5)
        console.log(number); //affiche number
    }
}
