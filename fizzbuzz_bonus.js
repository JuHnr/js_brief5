//BONUS

const listResult = document.querySelector(".result"); //récupère l'élément de classe .result dans le DOM


//fonction pour réaliser la boucle 

function fizzBuzz() {
    //création d'une variable pour stocker le résultat de chaque itération de la boucle
    let result = ""; 

    for (let i = 0; i <= 100; i++) {

        if (i % 3 === 0 && i % 5 === 0) {
            result = "FizzBuzz "; 

        } else if (i % 3 === 0) {
            result = "Fizz "; 

        } else if (i % 5 === 0) {
            result = "Buzz "; 

        } else {
            result = i; 
        }

        //à chaque itération, créé un <li>
        const listElement = document.createElement("li");

        //le <li> contient le résultat obtenu dans la condition
        listElement.textContent = result; 

        //le <li> est ajouté à listResult (<ul> est le parent de <li>)
        listResult.appendChild(listElement); 
    }

}

// Appel de la fonction fizzBuzz

fizzBuzz();
